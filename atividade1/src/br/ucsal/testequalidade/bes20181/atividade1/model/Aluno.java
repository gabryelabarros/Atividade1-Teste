package br.ucsal.testequalidade.bes20181.atividade1.model;


public class Aluno {

	private String nome;
	private int nota1;
	private int nota2;
	private int media;
	private String conceito;

	public String getConceito() {
		return conceito;
	}

	public void setConceito(String conceito) {
		this.conceito = conceito;
	}

	public String getNome() {
		return nome;
	}

	public int getNota1() {
		return nota1;
	}

	public void setNota1(int nota1) {
		this.nota1 = nota1;
	}

	public int getNota2() {
		return nota2;
	}

	public void setNota2(int nota2) {
		this.nota2 = nota2;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getMedia() {
		return media;
	}

	public void setMedia(int media) {
		this.media = media;
	}

}
