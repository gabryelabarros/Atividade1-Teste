package br.ucsal.testequalidade.bes20181.atividade1.model;

public class Peso {

	private int codigo;
	private int valor;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

}
