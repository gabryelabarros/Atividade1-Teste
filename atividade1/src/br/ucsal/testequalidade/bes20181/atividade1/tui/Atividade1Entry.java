package br.ucsal.testequalidade.bes20181.atividade1.tui;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.ucsal.testequalidade.bes20181.atividade1.business.RegistroNotaBC;
import br.ucsal.testequalidade.bes20181.atividade1.exceptions.PesoInvalidoException;
import br.ucsal.testequalidade.bes20181.atividade1.model.Aluno;
import br.ucsal.testequalidade.bes20181.atividade1.model.Peso;
import br.ucsal.testequalidade.bes20181.atividade1.persistence.RegistroNotaDAO;

public class Atividade1Entry {

	public static void main(String[] args) throws PesoInvalidoException {

		RegistroNotaTui registroNotaTui = new RegistroNotaTui();
		RegistroNotaBC registroBC = new RegistroNotaBC();

		
		List<Peso> pesoList = new ArrayList<>();
		Peso peso1 = registroBC.registarPesos(1, 2);
		Peso peso2 = registroBC.registarPesos(2, 1);
		pesoList.add(peso2);
		pesoList.add(peso1);
		
		
		Aluno aluno = new Aluno();
		registroNotaTui.registarNotas(aluno, 80, 70);
		aluno.setMedia(registroNotaTui.calcularMedia(aluno.getNota1(), aluno.getNota2(), peso1.getValor(), peso2.getValor()));
		String conceito = registroNotaTui.calcularConceito(aluno.getMedia());
		
		System.out.println("NOTA1 DO ALUNO: " + aluno.getNota1());
		System.out.println("NOTA2 DO ALUNO: " + aluno.getNota2());
		System.out.println("PESO DA NOTA1: " + peso1.getValor());
		System.out.println("PESO DA NOTA2: " + peso2.getValor());
		System.out.println("M�DIA DO ALUNO: " + aluno.getMedia());
		System.out.println("CONCEITO DO ALUNO: " +  conceito);
		

	}

}
