package br.ucsal.testequalidade.bes20181.atividade1.tui;

import br.ucsal.testequalidade.bes20181.atividade1.business.RegistroNotaBC;
import br.ucsal.testequalidade.bes20181.atividade1.exceptions.PesoInvalidoException;
import br.ucsal.testequalidade.bes20181.atividade1.model.Aluno;
import br.ucsal.testequalidade.bes20181.atividade1.model.Peso;

public class RegistroNotaTui {
	
	public RegistroNotaBC registroNotaBC = new RegistroNotaBC();
	
	public Peso registrarPesos(Integer codigoPeso, Integer valorPeso) throws PesoInvalidoException {
		return registroNotaBC.registarPesos(codigoPeso, valorPeso);
	}

	public Aluno registarNotas(Aluno aluno, Integer nota1, Integer nota2) {
		
		return registroNotaBC.registrarNotas(aluno, nota1, nota2);
	}
	
	public Integer calcularMedia (Integer nota1, Integer nota2, int peso1, int peso2) {
		
		return registroNotaBC.calcularMedia(nota1, nota2, peso1, peso2);
	}
	
	public String calcularConceito(Integer media){
		
		return registroNotaBC.calcularConceito(media);
	}

}
