package br.ucsal.testequalidade.bes20181.atividade1.test;

import org.junit.Test;

import br.ucsal.testequalidade.bes20181.atividade1.business.RegistroNotaBC;
import br.ucsal.testequalidade.bes20181.atividade1.exceptions.NotaInvalidaException;

public class ValidarNotaTest {

	public RegistroNotaBC registroBc = new RegistroNotaBC();

	@Test(expected = br.ucsal.testequalidade.bes20181.atividade1.exceptions.NotaInvalidaException.class)
	public void validarNotaAbaixoPermitido() throws NotaInvalidaException {
		int nota = -1;
		registroBc.validarNota(nota);
	}

	@Test(expected = br.ucsal.testequalidade.bes20181.atividade1.exceptions.NotaInvalidaException.class)
	public void validarNotaAcimaPermitido() throws NotaInvalidaException {
		int nota = 101;
		registroBc.validarNota(nota);
	}

	@Test
	public void validarNotaPermitida() throws NotaInvalidaException {
		int nota = 11;
		registroBc.validarNota(nota);
	}

}
