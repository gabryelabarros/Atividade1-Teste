package br.ucsal.testequalidade.bes20181.atividade1.test;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.testequalidade.bes20181.atividade1.business.RegistroNotaBC;

public class ValidarPesoTest {

	public RegistroNotaBC registroBc = new RegistroNotaBC();

	@Test
	public void validarPesoAbaixoInvalido() {
		int peso = -1;

		Assert.assertFalse(registroBc.validarPeso(peso));
	}

	@Test
	public void validarPesoAcimaInvalido() {
		int peso = 11;

		Assert.assertFalse(registroBc.validarPeso(peso));
	}
	
	@Test
	public void validarPesoValido() {
		int peso = 10;

		Assert.assertTrue(registroBc.validarPeso(peso));
	}

}
