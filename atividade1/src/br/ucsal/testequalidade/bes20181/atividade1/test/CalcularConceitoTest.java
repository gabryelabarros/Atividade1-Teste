package br.ucsal.testequalidade.bes20181.atividade1.test;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.testequalidade.bes20181.atividade1.business.RegistroNotaBC;

public class CalcularConceitoTest {

	public RegistroNotaBC registroBc = new RegistroNotaBC();

	@Test
	public void calcularConceitoOtimoLimiteInferior() {
		int mediaOtima = 75;
		String mensagemEsperada = "�timo! :D";
		String mensagemRetorno = registroBc.calcularConceito(mediaOtima);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoOtimoLimiteSuperior() {
		int mediaOtima = 100;
		String mensagemEsperada = "�timo! :D";
		String mensagemRetorno = registroBc.calcularConceito(mediaOtima);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoBomLimiteInferior() {
		int mediaBoa = 50;
		String mensagemEsperada = "Bom! :|";
		String mensagemRetorno = registroBc.calcularConceito(mediaBoa);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoBomLimiteSuperior() {
		int mediaBoa = 74;
		String mensagemEsperada = "Bom! :|";
		String mensagemRetorno = registroBc.calcularConceito(mediaBoa);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoRegularLimiteInferior() {
		int mediaRegular = 25;
		String mensagemEsperada = "Regular! :/";
		String mensagemRetorno = registroBc.calcularConceito(mediaRegular);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoRegularLimiteSuperior() {
		int mediaRegular = 49;
		String mensagemEsperada = "Regular! :/";
		String mensagemRetorno = registroBc.calcularConceito(mediaRegular);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoInsuficienteInferior() {
		int mediaInsuficiente = 0;
		String mensagemEsperada = "Insuficiente! :(";
		String mensagemRetorno = registroBc.calcularConceito(mediaInsuficiente);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

	@Test
	public void calcularConceitoInsuficienteSuperior() {
		int mediaInsuficiente = 24;
		String mensagemEsperada = "Insuficiente! :(";
		String mensagemRetorno = registroBc.calcularConceito(mediaInsuficiente);
		Assert.assertEquals(mensagemEsperada, mensagemRetorno);
	}

}
