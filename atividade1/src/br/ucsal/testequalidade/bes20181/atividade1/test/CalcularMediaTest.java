package br.ucsal.testequalidade.bes20181.atividade1.test;

import org.junit.Assert;
import org.junit.Test;

import br.ucsal.testequalidade.bes20181.atividade1.business.RegistroNotaBC;

public class CalcularMediaTest {

	
	public RegistroNotaBC registroBc = new RegistroNotaBC();
	
	
	@Test
	public void calcularMedia(){
		int mediaEsperada = 66;
		int resultadoMedia = registroBc.calcularMedia(70, 60, 2, 1);
		Assert.assertEquals(mediaEsperada, resultadoMedia);
	}
}
