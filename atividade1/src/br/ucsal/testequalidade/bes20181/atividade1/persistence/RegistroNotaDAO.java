package br.ucsal.testequalidade.bes20181.atividade1.persistence;

import br.ucsal.testequalidade.bes20181.atividade1.model.Aluno;
import br.ucsal.testequalidade.bes20181.atividade1.model.Peso;

public class RegistroNotaDAO {

	public Peso inserirPeso(Integer codigoPeso, Integer valor) {
		Peso peso = new Peso();
		peso.setCodigo(codigoPeso);
		peso.setValor(valor);
		return peso;

	}

	public Aluno inserirNota(Aluno aluno, Integer nota1, Integer nota2) {
		
		aluno.setNota1(nota1);
		aluno.setNota2(nota2);
		return aluno;
	}

}
