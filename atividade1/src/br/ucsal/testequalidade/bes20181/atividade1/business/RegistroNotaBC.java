package br.ucsal.testequalidade.bes20181.atividade1.business;

import br.ucsal.testequalidade.bes20181.atividade1.exceptions.NotaInvalidaException;
import br.ucsal.testequalidade.bes20181.atividade1.exceptions.PesoInvalidoException;
import br.ucsal.testequalidade.bes20181.atividade1.model.Aluno;
import br.ucsal.testequalidade.bes20181.atividade1.model.Peso;
import br.ucsal.testequalidade.bes20181.atividade1.persistence.RegistroNotaDAO;

public class RegistroNotaBC {

	public RegistroNotaDAO registroNotaDAO = new RegistroNotaDAO();

	public Peso registarPesos(Integer codigoPeso, Integer valorPeso) throws PesoInvalidoException {
		if (validarPeso(valorPeso)) {
			return registroNotaDAO.inserirPeso(codigoPeso, valorPeso);
		} else {
			throw new PesoInvalidoException();
		}
	}

	public Aluno registrarNotas(Aluno aluno, Integer nota1, Integer nota2) {

		try {
			validarNota(nota1);
			validarNota(nota2);
		} catch (NotaInvalidaException e) {
			e.printStackTrace();
		}
		return registroNotaDAO.inserirNota(aluno, nota1, nota2);
	}

	public boolean validarPeso(Integer peso) {
		boolean ehValido = false;
		if (peso >= 1 && peso <= 10) {
			ehValido = true;
		}

		return ehValido;
	}

	public void validarNota(Integer nota) throws NotaInvalidaException {
		if (nota == null || nota < 0 || nota > 100) {
			throw new NotaInvalidaException();
		}
	}

	public Integer calcularMedia(Integer nota1, Integer nota2, int peso1, int peso2) {
		int media;
		media = ((nota1 * peso1) + (nota2 * peso2)) / (peso1 + peso2);
		return media;
	}

	public String calcularConceito(Integer media) {
		String mensagem = "";
		if (media >= 0 && media <= 24) {
			mensagem = "Insuficiente! :(";
		} else if (media >= 25 && media <= 49) {
			mensagem = "Regular! :/";
		} else if (media >= 50 && media <= 74) {
			mensagem = "Bom! :|";
		} else if (media >= 75 && media <= 100) {
			mensagem = "�timo! :D";
		}
		return mensagem;
	}


}
